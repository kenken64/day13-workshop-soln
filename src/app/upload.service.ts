import { Injectable } from '@angular/core';
import { HttpClient, HttpEventType } from  '@angular/common/http';
import { map } from  'rxjs/operators';
import { environment } from '../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class UploadService {
  constructor(private httpClient: HttpClient) { }

  public upload(data) {
    let uploadURL = `${environment.api_url}/upload`;
    return this.httpClient.post<any>(uploadURL, data);
  }
}
