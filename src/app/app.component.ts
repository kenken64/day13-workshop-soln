import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from  '@angular/forms';
import { UploadService } from  './upload.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{
  title = 'day13-workshop-soln';
  form: FormGroup;
  
  constructor(private formBuilder: FormBuilder, private uploadService: UploadService){}

  ngOnInit() {
    this.form = this.formBuilder.group({
      myphoto: ['']
    });
  }

  onFileChange(event) {
    if (event.target.files.length > 0) {
      const file = event.target.files[0];
      this.form.get('myphoto').setValue(file);
    }
  }

  onSubmit() {
    const formData = new FormData();
    formData.append('myphoto', this.form.get('myphoto').value);

    this.uploadService.upload(formData).subscribe(
      (res) => console.log(res),
      (err) => console.log(err)
    );
  }
}
