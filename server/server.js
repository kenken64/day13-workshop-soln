require('dotenv').config();
//import
const express = require('express'),
      exphb = require('express-handlebars'),
      multer = require('multer'),
      path = require('path'),
      fs = require('fs'),
      cors = require('cors'),
      compression = require('compression'),
      asciify = require('asciify-image');

// init
const app = express();
const APP_PORT = process.env.PORT;

// Setup handlebar
app.engine('hbs', exphb());
app.set('view engine', 'hbs');
app.use(compression());
app.use(cors());
// initialize static content
app.use(express.static(path.join(__dirname, 'public')));
finalImageDir = path.join(__dirname, 'views', 'images');

const storage = multer.diskStorage({
    destination: (req,file,callback)=>{
        console.log(file);
        callback(null, path.join(__dirname, 'views', 'images'))
    },
    filename: (req, file, callback)=>{
        console.log(file);
        callback(null, Date.now() + '-' + file.originalname);
    }
})
const upload = multer({storage:storage});


app.use(express.static(finalImageDir));
// img tag
var randomImagesArr = [];
// physical path
var randomImagesArrwithPath = [];
// jpeg only photos
var randomImagesArrJpgOnly = [];
// read all the the files upfront before the end point is being access
// cache it to the defined array above
fs.readdir(finalImageDir, function(err, filenames) {
    if (err) {
      return;
    }
    filenames.forEach(function(filename) {
        randomImagesArr.push(filename);
        let filterImage = filename.split('.');
        if(filterImage[1] === 'jpg'){
            randomImagesArrJpgOnly.push(path.join(finalImageDir, filename));
        }else{
            randomImagesArrwithPath.push(path.join(finalImageDir, filename));
        }
    });
});

app.post('/upload', upload.single('myphoto'), (req,res, next)=>{
    console.log(req.file);
    console.log(req.body);
    randomImagesArr,randomImagesArrwithPath, randomImagesArrJpgOnly = [];
    fs.readdir(finalImageDir, function(err, filenames) {
        if (err) {
          return;
        }
        filenames.forEach(function(filename) {
            randomImagesArr.push(filename);
            let filterImage = filename.split('.');
            if(filterImage[1].toUpperCase() === 'jpg'.toUpperCase() 
                ||  filterImage[1].toUpperCase() === 'jpeg'.toUpperCase() ){
                randomImagesArrJpgOnly.push(path.join(finalImageDir, filename));
            }else{
                randomImagesArrwithPath.push(path.join(finalImageDir, filename));
            }
        });
        console.log(randomImagesArrJpgOnly);
        res.status(200).json({message: 'OK'});
    });
});


// helper random function
function getRandomInt(max) {
    return Math.floor(Math.random() * Math.floor(max));
}

app.get('/image', (req,res,next)=>{
    let randomNumber = getRandomInt(randomImagesArr.length);
    res.status(200);
    res.format({
        'text/html': ()=>{
            console.log("returning html");
            res.type('text/plain');
            res.send(`<img src="${randomImagesArr[randomNumber]}"></img>`);
        },
        'application/json': ()=>{
            console.log("returning json");
            res.type('application/json');
            res.json({image: randomImagesArr[randomNumber]});
        },
        'images/jpg': ()=>{
            console.log("returning jpeg " + randomImagesArrJpgOnly);
            res.type('images/jpg');
            res.sendFile(randomImagesArrJpgOnly[randomNumber]);
        },
        'text/plain': ()=>{
            console.log("returning text");
            res.type('text/plain');
            var options = {
                fit:    'box',
                width:  200,
                height: 100
              }
               
            asciify(randomImagesArrwithPath[randomNumber], options)
            .then(function (asciified) {
                // Print asciified image to console
                console.log(asciified);
                res.send(asciified);
            })
            .catch(function (err) {
                // Print error to console
                console.error(err);
            });
        },
        'default': ()=>{
            console.log("returning default");
            res.status(406).send('Not Acceptable');
        }
    });

});

// endpoint uses hbs instead of returning html structure
app.get('/image-hbs', (req,res,next)=>{
    let randomNumber = getRandomInt(randomImagesArr.length);
    res.render('randomimage',{randomImageURL: randomImagesArr[randomNumber]});
});

// randomize from the file system.
app.get('/random-image', (req,res,next)=>{
    let randomNumber = getRandomInt(randomImagesArrwithPath.length);
    res.sendFile(randomImagesArrwithPath[randomNumber]);
})

app.use((req, res, next)=>{
    res.send("<b>Not found !</b>")
})

app.listen(APP_PORT, ()=>{
    console.log(`App Server started on ${APP_PORT}`)
})
